# Scan Image Time Spectroscopy

This project aims at providing easy-to-use and readily available support for realizing time-spectroscopy experiment for point scanning microscopy aided by the commercial software ScanImage
## Introduction : 

ScanImage is a scanning microscopy software allowing for fast and optimized control of galvo-mirrors systems as well as multiple other options. In this repository, we share a matlab GUI used to implement time-spectroscopy in this framework. For this, we use different RF signals emitted from the DAQ of ScanImage to synchronize a time-scan for each pixel. This can be extended to the control of any time variation during the time-scale of a pixel dwell time.  

This software is hence made for three dimensionnal imaging using a scanning microscope with a third parameter varying during the scanning of a single pixel. Please refer to the (upcoming) publication for more details. 

## Requirements : 
- [ ] All the following codes have been redacted on Matlab with versions between 2021-2023. Back compatibility is insured in Matlab but warnings may arise. 
- [ ] The commercial software ScanImage is absolutely necessary. It is the basis of controlling many different objects and is a core part of this project. (Our version is : Basic 2022.0.0 29e163d768)
- [ ] 

## Organization :
The project provides multiple parts : 
- [ ] A code-processing class that allows to do simple time-processing step such as Fourier Transform Time spectroscopy, windowing etc... 
        (RmFast.m, RmFast_Struct.m)
- [ ] Mechanical delay line control classes that allow to control the mechanical delay lines that were used in the original iteration of this project
        (Benchtop_test.m, PI_C663.m)
- [ ] a Matlab App that integrates all parts with ScanImage and provide a simplified user interface for ease of utilization. 
        (Time_Spectro_For_PUBLICATION.mlapp)

The mlapp file can be open with the app designer from Matlab to change the different parameters inside. There should be just a few, such as : 
    - the path to scan image. 
    - 

 The mechanical delay line codes can be modified but are provided as necessary for the functionning of this repository. They should work in any environment to adress the following mechanical delay lines : 
    Thorlabs Brushless Benchtop 
    https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=5066
    PI C-663.12 Mercury Step Stepper Motor Controller, 1 Axis
     https://www.pifrance.fr/fr/products/controllers-and-drivers/motion-controllers-drivers-for-linear-torque-stepper-dc-servo-motors/c-66312-mercury-step-stepper-motor-controller-900553

The data processing class is used to realize fast fourier transform for time-spectroscopy and region of interest selection. It can however be adapted to realize any processing on your own time signals, fast enough to garantee close to 10 Hz real time processing on your images. The principal developper of this code has a manual on their own git : https://github.com/paulohdiniz/RamanScattering/tree/main
## Hierarchy : 
The application launches SI(ScanImage) on its own when started, it then stores the important classes as properties, and uses them to communicate with SI. Modification of the imaging parameters in the app are propagated own to SI, but not the other way around. One can still use SI independantly of the app as it is running, but errors may arise in the app yet, not impeding SI. We recommend using only the buttons in the GUI once your framework has been properly set up. 
## Get Started : 
NEED TO INSTALL KINESIS FOR THE BENCHTOP TO WORK 
Download the app and the following files and open it in the app designer of Matlab. 

## Graphical User Interface breakdown :
In the Design view, one can see the different parts of the app and the workflow.

![App screenshot](/Images_Readme/Full_app.png)

The different panels centralize the different controls needed for basic imaging. 
* The Acquisition Panel gives the current state of the acquisition and allows to "GRAB" (grab one image), "FOCUS" (acquire continuously), or "ABORT" the acquisition. 
- The Save Panel : clicking the "Save Image" box will lead to tiff files creation in the folder choosen in the "Choose Folder" button with the name entered in the blank field "Sample". The "Average" button will enable the successive acquisition of multiple images for a single averaged image to be stored in the Tiff file. The number of averages can be determined above. 
- The different Channels of acquisition can be readressed below, with the Time Channel being the one connected to your detector that follow the time dependance on one pixel, the Transmission channel connected to the detector that collects the instantaneous transmission, if it differs, and the last channel is for the triggers emitted by the wave generators to synchronize the data_processing. 
 - The Motor controls channel allows for the movement in XYZ and delay if there is a delay line selected. 
 - The Image Parameters controls the number of point for each line (Nx) and the number of lines (Ny) as well as the number of points taken on the time axis (Nt). The FOV gives an approximation of the measured field of view compared to the Zoom factor of SI. This needs to be calibrated on each different setup. 
 - The Timing Panel




In the code view, one can get an idea of the different functions and callbacks used. Here is a succint description of their different workings : 

Start_up_Function : 
    Initialize the app, launches scan_image, and syncronizes the different parameters in the app to their counter part in scan image. 




## Known Bugs at installation 

Kinesis DLLs : 
The properties associated with the thorlabs delay line are instantiated line 262 and 263. In the absence of these DLLs, the app won't launch. The solution is to download the Kinesis software from this adress : https://www.thorlabs.com/software_pages/ViewSoftwarePage.cfm?Code=Motion_Control
and edit MOTORPATHDEFAULT in the Benchtop.m file to point to the location of the DLLs. (Default : C:\Program Files\Thorlabs\Kinesis\)




## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.fresnel.fr/metais/scan-image-time-spectroscopy.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.fresnel.fr/metais/scan-image-time-spectroscopy/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
