classdef Benchtop < handle
    % Matlab class to control Thorlabs motorised rotation stages
    % It is a 'wrapper' to control Thorlabs devices via the Thorlabs .NET
    % DLLs.
    %
    % Instructions:
    % Download the Kinesis DLLs from the Thorlabs website from:
    % https://www.thorlabs.com/software_pages/ViewSoftwarePage.cfm?Code=Motion_Control
    % Edit MOTORPATHDEFAULT below to point to the location of the DLLs
    % Connect your PRM1Z8 and/or K10CR1 rotation stage(s) to the PC USB port(if
    % using PRMZ8 also switch it on)
    %
    % Example:
    % a=Benchtop.listdevices()   % List connected devices
    % m1=Benchtop          % Create a motor object
    % connect_channel_1(m1,a{1})      % Connect the first Channel of the first device in the list of devices
    % home(m1)              % Home the device
    % moveto(m1,45)         % Move the device to the 45 degree setting
    % moverel_deviceunit(m1, -100000) % Move 100000 'clicks' backwards
    % disconnect(m1)        % Disconnect device
    %
    % Adapted by Samuel Metais from previous code from :
    % Author: Julan A.J. Fells
    % Dept. Engineering Science, University of Oxford, Oxford OX1 3PJ, UK
    % Email: julian.fells@emg.ox.ac.uk (please email issues and bugs)
    % Website: http://wwww.eng.ox.ac.uk/smp
    %
    % Known Issues:
    % 1. If motor object gets deleted or corrupted it is sometimes necessary to
    % restart Matlab
    %
    % Version History:
    % 1.0 14 March 2018 First Release


    properties (Constant, Hidden)
        % path to DLL files (edit as appropriate)
        MOTORPATHDEFAULT='C:\Program Files\Thorlabs\Kinesis\'
        % DLL files to be loaded
        DEVICEMANAGERDLL='Thorlabs.MotionControl.DeviceManagerCLI.dll';
        DEVICEMANAGERCLASSNAME='Thorlabs.MotionControl.DeviceManagerCLI.DeviceManagerCLI'
        GENERICMOTORDLL='Thorlabs.MotionControl.GenericMotorCLI.dll';
        GENERICMOTORCLASSNAME='Thorlabs.MotionControl.GenericMotorCLI.GenericMotorCLI';
        DCSERVODLL='Thorlabs.MotionControl.KCube.DCServoCLI.dll';
        DCSERVOCLASSNAME='Thorlabs.MotionControl.KCube.DCServoCLI.KCubeDCServo';
        BENCHTOPBRUSHLESS='Thorlabs.MotionControl.Benchtop.BrushlessMotorCLI.dll';
        INTEGSTEPDLL='Thorlabs.MotionControl.IntegratedStepperMotorsCLI.dll'
        INTEGSTEPCLASSNAME='Thorlabs.MotionControl.IntegratedStepperMotorsCLI.IntegratedStepperMotor.CageRotator';
        % Default intitial parameters
        DEFAULTVEL=10;           % Default velocity
        DEFAULTACC=10;           % Default acceleration
        TPOLLING=250;            % Default polling time
        TIMEOUTSETTINGS=7000;    % Default timeout time for settings change
        TIMEOUTMOVE=10000;      % Default time out time for motor move
    end
    properties
        % These properties are within Matlab wrapper
        isconnected=false;           % Flag set if device connected
        serialnumber;                % Device serial number
        controllername;              % Controller Name
        controllerdescription        % Controller Description
        stagename;                   % Stage Name
        position;                    % Position
        acceleration;                % Acceleration
        maxvelocity;                 % Maximum velocity limit
        minvelocity;                 % Minimum velocity limit
        Channel                      % The channel we are using
        Channel_Conf                 % Channel Configuration
        Delay_0=220;                      % When used as a delay line, time of pulse overlap, set by default at the minimum motor can achieve
        c=299792458;                 % speed of light m/s
        n_air=1.00027;               % refractive index of air at 800 nm at 15 C
    end
    properties (Hidden)
        % These are properties within the .NET environment.
        deviceNET;                   % Device object within .NET
        BenchtopSettingsNET;            % motorSettings within .NET
        currentDeviceSettingsNET;    % currentDeviceSetings within .NET
        deviceInfoNET;               % deviceInfo within .NET

    end
    methods

        function init(obj)
            a=Benchtop.listdevices();  % List connected devices
            connect_channel_1(obj,a{1})  ;
        end

        function goHome(h) %Test function to find the different function we will use
            h.Channel.Home(60000) % Does not tell when finished
            while ~h.Channel.Status.IsHomed()
                pause(1)
                disp('Homing')
            end
            clc
            disp('Homing Finished')
        end

        function set_overlap(h)
            h.Delay_0=System.Decimal.ToDouble(h.Channel.Position());
            disp('Overlap delay set.')
        end

        function h=Benchtop_test()  % Instantiate motor object
            Benchtop.loaddlls; % Load DLLs (if not already loaded)
        end

        function connect(h,serialNo)  % Connect device to the Benchtop
            h.listdevices();    % Use this call to build a device list in case not invoked beforehand
            if ~h.isconnected
                h.deviceNET=Thorlabs.MotionControl.Benchtop.BrushlessMotorCLI.BenchtopBrushlessMotor.CreateBenchtopBrushlessMotor(serialNo);
                h.deviceNET.Connect(serialNo);          % Connect to device via .NET interface
                h.isconnected=h.deviceNET.IsConnected();
                disp('Connection Successful')
            else % Device is already connected
                error('Device is already connected.')
            end
            %             updatestatus(h);   % Update status variables from device

        end

        function connect_channel_1(h,serialNo)  % Connect device to the Benchtop
            h.listdevices();    % Use this call to build a device list in case not invoked beforehand
            if ~h.isconnected
                h.deviceNET=Thorlabs.MotionControl.Benchtop.BrushlessMotorCLI.BenchtopBrushlessMotor.CreateBenchtopBrushlessMotor(serialNo);
                h.deviceNET.Connect(serialNo);  % Connect to device via .NET interface
                h.serialnumber=serialNo;
                h.isconnected=h.deviceNET.IsConnected(); % Check connection and store it
                h.Channel= h.deviceNET.GetChannel(1); %Connects to the actual actuator of channel 1

                disp('Connection to Channel 1 Successful')
                if ~h.Channel.IsSettingsInitialized() % Initialization
                    h.Channel.WaitForSettingsInitialized(5000)
                end
                h.Channel.StartPolling(250);
                pause(0.5)
                h.Channel.EnableDevice(); % Enabling
                pause(0.5)
                h.Channel_Conf=h.Channel.LoadMotorConfiguration(h.Channel.DeviceID); % Loading Configuration

            else % Device is already connected
                error('Device is already connected.')
            end
        end

        function moveto(h,position)     % Move to absolute position
            try
                h.Channel.MoveTo(position,60000)       % Move devce to position via .NET interface
                disp(['Delay Line moved to ' num2str(System.Decimal.ToDouble(h.Channel.Position()))])
            catch % Device failed to move
                error(['Unable to Move device ',h.serialnumber,' to ',num2str(position)]);
            end
        end

        function moveto_rel(h,position)     % Move to absolute position
            cur_pos=System.Decimal.ToDouble(h.Channel.Position());
            new_pos=cur_pos+position;
            try
                h.Channel.MoveTo( new_pos,60000)       % Move device to position via .NET interface
                disp(['Delay Line moved to ' num2str(System.Decimal.ToDouble(h.Channel.Position()))])
            catch % Device failed to move
                error(['Unable to Move device ',h.serialnumber,' to ',num2str(position)]);
            end
        end

        function move_delay_rel(h,delay)
            cur_pos=System.Decimal.ToDouble(h.Channel.Position());
            new_pos=cur_pos+delay*1e-12*h.c/h.n_air*1e3/2;
            if (0<new_pos) && (new_pos<220)
                try
                    h.Channel.MoveTo(new_pos,60000)       % Move device to position via .NET interface
                    disp(['Delay Line moved to ' num2str(System.Decimal.ToDouble(h.Channel.Position()))])
                    disp(['Delay (ps) moved to ' num2str((System.Decimal.ToDouble(h.Channel.Position())-h.Delay_0)*1e-3*h.n_air/h.c*1e12*2)])
                catch % Device failed to move
                    error(['Unable to Move device ',h.serialnumber,' to ',num2str(new_pos)]);
                end
            else
                disp('Outside Motor Range')
            end
        end

%       function Check_time_overlap(m1)
%             moveto(m1,85)
% %             Initial_position=h.Channel.Position();
%             setvelocity(m1,0.1,.1);
%             for i=85:0.001:86
%               moveto(m1,i)
%               pause(0.2)
%             end
%         end

        function move_delay(h,delay)          
            new_pos=h.Delay_0+delay*1e-12*h.c/h.n_air*1e3/2;
            try
                h.Channel.MoveTo(new_pos,60000)       % Move device to position via .NET interface
                disp(['Delay Line moved to ' num2str(System.Decimal.ToDouble(h.Channel.Position()))])
                disp(['Delay (ps) moved to ' num2str((System.Decimal.ToDouble(h.Channel.Position())-h.Delay_0)*1e-3*h.n_air/h.c*1e12*2)])
            catch % Device failed to move
                error(['Unable to Move device ',h.serialnumber,' to ',num2str(new_pos)]);
            end

        end



        %         function movecont(h, varargin)  % Set motor to move continuously
        %             if (nargin>1) && (varargin{1})      % if parameter given (e.g. 1) move backwards
        %                 motordirection=Thorlabs.MotionControl.GenericMotorCLI.MotorDirection.Backward;
        %             else                                % if no parametr given move forwards
        %                 motordirection=Thorlabs.MotionControl.GenericMotorCLI.MotorDirection.Forward;
        %             end
        %             h.deviceNET.MoveContinuous(motordirection); % Set motor into continous move via .NET interface
        %             updatestatus(h);            % Update status variables from device
        %         end
        function stop(h) % Stop the motor moving (needed if set motor to continous)
            h.deviceNET.Stop(h.TIMEOUTMOVE); % Stop motor movement via.NET interface
            updatestatus(h);            % Update status variables from device
        end
        %         function updatestatus(h) % Update recorded device parameters in matlab by reading them from the devuce
        %             h.isconnected=boolean(h.deviceNET.IsConnected());   % update isconncted flag
        %             h.serialnumber=char(h.deviceNET.DeviceID);          % update serial number
        %             h.controllername=char(h.deviceInfoNET.Name);        % update controleller name
        %             h.controllerdescription=char(h.deviceInfoNET.Description);  % update controller description
        %             h.stagename=char(h.BenchtopSettingsNET.DeviceSettingsName);    % update stagename
        %             velocityparams=h.deviceNET.GetVelocityParams();             % update velocity parameter
        %             h.acceleration=System.Decimal.ToDouble(velocityparams.Acceleration); % update acceleration parameter
        %             h.maxvelocity=System.Decimal.ToDouble(velocityparams.MaxVelocity);   % update max velocit parameter
        %             h.minvelocity=System.Decimal.ToDouble(velocityparams.MinVelocity);   % update Min velocity parameter
        %             h.position=System.Decimal.ToDouble(h.deviceNET.Position);   % Read current device position
        %         end
        function setvelocity(h, varargin)  % Set velocity and acceleration parameters
            velpars=h.Channel.GetVelocityParams(); % Get existing velocity and acceleration parameters
            switch(nargin)
                case 1  % If no parameters specified, set both velocity and acceleration to default values
                    velpars.MaxVelocity=h.DEFAULTVEL;
                    velpars.Acceleration=h.DEFAULTACC;
                case 2  % If just one parameter, set the velocity
                    velpars.MaxVelocity=varargin{1};
                case 3  % If two parameters, set both velocitu and acceleration
                    velpars.MaxVelocity=varargin{1};  % Set velocity parameter via .NET interface
                    velpars.Acceleration=varargin{2}; % Set acceleration parameter via .NET interface
            end
            if System.Decimal.ToDouble(velpars.MaxVelocity)>25  % Allow velocity to be outside range, but issue warning
                warning('Velocity >25 deg/sec outside specification')
            end
            if System.Decimal.ToDouble(velpars.Acceleration)>25 % Allow acceleration to be outside range, but issue warning
                warning('Acceleration >25 deg/sec2 outside specification')
            end
            h.Channel.SetVelocityParams(velpars); % Set velocity and acceleration paraneters via .NET interface
        end

        function disconnect(h) % Disconnect device
            h.isconnected=h.deviceNET.IsConnected(); % Update isconnected flag via .NET interface
            if h.isconnected
                try
                    h.deviceNET.Disconnect();   % Disconnect device via .NET interface
                catch
                    error(['Unable to disconnect device',h.serialnumber]);
                end
                h.isconnected=false;  % Update internal flag to say device is no longer connected
            else % Cannot disconnect because device not connected
                error('Device not connected.')
            end
        end
    end
    methods (Static)
        function serialNumbers=listdevices()  % Read a list of serial number of connected devices
            Benchtop.loaddlls; % Load DLLs
            Thorlabs.MotionControl.DeviceManagerCLI.DeviceManagerCLI.BuildDeviceList();  % Build device list
            serialNumbersNet = Thorlabs.MotionControl.DeviceManagerCLI.DeviceManagerCLI.GetDeviceList(); % Get device list
            serialNumbers=cell(ToArray(serialNumbersNet)); % Convert serial numbers to cell array
        end
        function loaddlls() % Load DLLs
            if ~exist(Benchtop.DEVICEMANAGERCLASSNAME,'class')
                try   % Load in DLLs if not already loaded
                    NET.addAssembly([Benchtop.MOTORPATHDEFAULT,Benchtop.DEVICEMANAGERDLL]);
                    NET.addAssembly([Benchtop.MOTORPATHDEFAULT,Benchtop.GENERICMOTORDLL]);
                    %                     NET.addAssembly([motor.MOTORPATHDEFAULT,motor.DCSERVODLL]);
                    %                     NET.addAssembly([motor.MOTORPATHDEFAULT,motor.INTEGSTEPDLL]);
                    NET.addAssembly([Benchtop.MOTORPATHDEFAULT,Benchtop.BENCHTOPBRUSHLESS]);
                catch % DLLs did not load
                    error('Unable to load .NET assemblies')
                end
            end
        end
    end
end

