classdef PI_C663_test <handle
    % Code for controlling the delay line PIM-415.2S
    % communicated with through a PI C-663 module.

    % This classes is a used to control the motor using a set of ASCII
    % commands sent through a serial port.
    properties (Constant = true)
        port = 'COM4'; % Serial port number (COM4 on vidrio computer) (check for PI C-663 in the device manager)
        baudRate = 38400; % Baud rate (must also be set physically on the DC-Motor controller zith pins 5(OFF) and 6(ON))
        terminator = 'LF' % 'CR' or 'LF' ('LF'is the one to go for PI C-663)
    end

    properties (GetAccess = public, SetAccess = protected)
        isconnected;
        ser % Serial object
        pos % Motor position
        err % Motor error position
        tar % Motor target position
        ana % Motor analog value
        velocity = 1.50000; % mm/s max 3.5
        maxtravel = 150; % Maximum heigth value (motor relative position cannot be higher)
        mintravel = 0; % Minimal heigth value (motor relative position cannot be lower)
        stage_name = '1' ; % Total step range (can be recalibrated)
        %         cal_posRange % Positive step range (defined during calibration)
        %         cal_negRange % Negative step range (defined during calibration)
        Delay_0;
        c=299792458;                 % speed of light m/s
        n_air=1.00027;               % refractive index of air at 800 nm at 15 C

    end

    methods

        function init(obj)
            obj.connect();
%             obj.sendCmd([ 'VEL ' obj.stage_name ' ' num2str(1.5)])
              obj.init_motor();

        end

        function goHome(obj)
            % Moves the motor to the home position
            out = obj.sendCmd('GOH');
            motion_test = sendCmd(obj,'ONT?');
            fprintf('\t > Motor moving to home...\n ')
            while strcmp(motion_test,'1=0')
                motion_test = sendCmd_noprint(obj,'ONT?');
                pause(0.05)
            end
            fprintf('done! \n')
        end

        function set_overlap(obj)
            obj.Delay_0 = getPos(obj,'mm');
        end

        function connect(obj) 
            % Constructor of the MercuryC862 class
            % Initiates the COM ports and starts it

            % Creates the serial port
            obj.ser = serial(obj.port,'BaudRate',obj.baudRate,'Terminator',obj.terminator, ...
                'ReadAsyncMode','continuous','FlowControl','Software');

            % Open it if it is closed
            if strcmp(obj.ser.Status,'closed')
                fopen(obj.ser)
            end

            % Check connection\
            Name_of_motor = obj.sendCmd('CST?');
            Name_of_motor
            % Check that we have the correct stage name for later
            out = obj.sendCmd('CST?');
            obj.stage_name=out(1);

            if strcmp(obj.ser.Status,'open')
                obj.isconnected = true;
            end
        end

        function out = sendCmd(obj,cmdstr)
            % Passes an ASCII command string to the Mercury C862.10,
            % executes it, and returns available data (if any)

            % Send command to the DC motor controller and wait for proper execution
            fprintf(obj.ser,cmdstr)
            pause(0.05)

            if obj.ser.BytesAvailable==0 % Tell that the command executed correctly if no output
                out = [cmdstr ', done'];
            else % Read available data in ASCII format, and prepare it
                out = fread(obj.ser,[1,obj.ser.BytesAvailable]);
                % Remove useless chars (line breaks, etc)
                out(out==3) = [];
                out(out==10) = [];
                out(out==13) = [];
                % Convert to char
                out = char(out);
            end
        end

        function out = sendCmd_noprint(obj,cmdstr)
            % Passes an ASCII command string to the Mercury C862.10,
            % executes it, and returns available data (if any)

            % Send command to the DC motor controller and wait for proper execution
            fprintf(obj.ser,cmdstr)
            pause(0.05)

            if obj.ser.BytesAvailable==0 % Tell that the command executed correctly if no output
                out = [cmdstr ', done'];
            else % Read available data in ASCII format, and prepare it
                out = fread(obj.ser,[1,obj.ser.BytesAvailable]);
                % Remove useless chars (line breaks, etc)
                out(out==3) = [];
                out(out==10) = [];
                out(out==13) = [];
                % Convert to char
                out = char(out);
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
           function init_motor(obj)
           test_servo= obj.sendCmd(['SVO? ' obj.stage_name]);
           if ~strcmp(test_servo,'1=1')
            obj.sendCmd(['SVO ' obj.stage_name ' 1'])
            fprintf('\t > SERVO ON \n ')
           end

            %Check if motor is properly referenced :
            test_ref = sendCmd(obj,'FRF?');
            %Then ref the axis if it is not referenced
            if strcmp(test_ref,'1=0')
                out = sendCmd(obj,'FPL');
                motion_test = sendCmd(obj,'ONT?');
                fprintf('\t > Motor Referencing...\n ')
                while strcmp(motion_test,'1=0')
                    motion_test = sendCmd_noprint(obj,'ONT?');
                    pause(0.05)
                end
               
            end
            % Show it is ready

            fprintf('\t > Motor Referenced.\n ')

        end

        function out = getPos(obj,unit)
            a=1;
            %get position of delay line in mm or in ps.
            out = obj.sendCmd('POS?');
            % transform to position on x axis
            out(1:2) = [];
            out = str2num(out);
            % Convert to number
            switch unit

                case 'mm'

                case 'ps'
                    out=out*1e-3*2/3e8*1e12;
                otherwise
                    disp('Error on unit')

            end
        end

        function move(obj,value,type)
            % Moves the motor, either absolutely from the home position,
            % or relatively from the current position
            % Get current position (to further check for unallowed movements)
            cur_pos=obj.getPos('mm');
            % Assign the correct movement string according to the movement type
            % Check for unallowed movements based on current position and min max values
            switch lower(type)
                case 'abs'
                    if value >= 0
                        if value > obj.maxtravel
                            error('Movement is aborted (motor would end up higher than max limit)');
                        end
                    else % value < 0
                        if value < obj.mintravel
                            error('Movement is aborted (motor would end up lower than min limit)');
                        end
                    end
                    % If there is no error, assing the 'MOV' string to mvStr
                    mvStr = 'MOV';
                    cmdStr=[mvStr ' ' obj.stage_name ' ' num2str(value)];
                case 'rel'
                    if value >= 0
                        if (cur_pos + abs(value)) > obj.maxtravel
                            error('Movement is aborted (motor would end up higher than max limit)');
                        end
                    else % value < 0
                        if (cur_pos - abs(value)) < obj.mintravel
                            error('Movement is aborted (motor would end up lower than min limit)');
                        end
                    end
                    % If there is no error, assing the 'MR' string to mvStr
                    mvStr = 'MVR';    
                    if value>=0
                         cmdStr=[mvStr ' ' obj.stage_name ' ' num2str(value)];
                    else
                        cmdStr=[mvStr ' ' obj.stage_name ' ' num2str(value)];

                    end
                otherwise
                    error('Undefined movement type, choose between ''abs'' and ''rel''!')
            end
            out = obj.sendCmd(cmdStr);
        end

        function move_abs(obj, value)
            move(obj, value, 'abs');
        end

        function move_rel(obj, value)
            move(obj, value, 'rel');
        end

        function move_delay(h, delay)
            new_pos=h.Delay_0+delay*1e-12*h.c/h.n_air*1e3/2;
            try
                h.move_abs(new_pos)       
                disp(['Delay Line moved to ' num2str(getPos(h,'mm'))])
                disp(['Delay (ps) moved to ' num2str((getPos(h,'mm')-h.Delay_0)*1e-3*h.n_air/h.c*1e12*2)])
            catch % Device failed to move
                error(['Unable to Move device ',h.ser.Port,' to ',num2str(new_pos)]);
            end

        end

        function stop(obj)
             obj.sendCmd([ 'HLT ']);
        end

        function disconnect(obj) % destructor
            fclose(obj.ser);
            delete(obj.ser);
        end % desctructor
    end

end


